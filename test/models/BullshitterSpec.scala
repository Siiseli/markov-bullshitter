package models

import service.Bullshitter
import test.BaseSpec

import scala.util.Random
import scala.collection.mutable.{ArrayBuffer, Map}

class BullshitterSpec extends BaseSpec {

  "bullshit" should {

    "accept source string as parameter" in {
      new Bullshitter(scala.util.Random).bullshit("a b c", "a b") mustBe "a b c"
    }

    "traverse through an unbranching markov chain" in {
      new Bullshitter(scala.util.Random).bullshit(Map("a b" -> ArrayBuffer("c")), "a b") mustBe "a b c"
    }

    "traverse through a simple branching markov chain" in {
      val randomMock = mock[Random]
      (randomMock.nextInt (_: Int)).expects(2).returns(1);

      new Bullshitter(randomMock).bullshit(Map("a b" -> ArrayBuffer("c", "k")), "a b") mustBe "a b k"
    }

    "stop when it encounters a period" in {
      new Bullshitter(scala.util.Random).bullshit(
        Map(
          "a b" -> ArrayBuffer("c."),
          "b c." -> ArrayBuffer("b", "d")), "a b") mustBe "a b c."
    }

    "traverse through a complex branching markov chain" in {
      val randomMock = mock[Random]
      (randomMock.nextInt (_: Int)).expects(2).anyNumberOfTimes().returns(1);

      new Bullshitter(randomMock).bullshit(
        Map(
          "a b" -> ArrayBuffer("c", "k"),
          "b k" -> ArrayBuffer("a", "c"),
          "k a" -> ArrayBuffer("d", "e")), "a b") mustBe "a b k c"
    }
  }

  "createDirectory" should {

    "create directory from source string" in {
      val expectedMap = Map(
        "a b" -> ArrayBuffer("c","k"),
        "b c" -> ArrayBuffer("a"),
        "c a" -> ArrayBuffer("b"))
      new Bullshitter(scala.util.Random).createDirectory("a b c a b k") mustBe expectedMap
    }
  }

}
