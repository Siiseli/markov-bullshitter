package service

import scala.collection.mutable.{ArrayBuffer, Map}
import scala.util.Random

class Bullshitter(random : Random) {

  def bullshit(source : String, startingPoint : String) : String = {
    bullshit(createDirectory(source), startingPoint)
  }

  def bullshit(directory : Map[String, ArrayBuffer[String]], startingPoint : String) : String = {
    var currentKey = startingPoint

    var results = startingPoint

    while(directory.contains(currentKey) && !shouldStop(currentKey)) {
      val possibleContinuations : ArrayBuffer[String] = directory(currentKey)
      val nextWord = possibleContinuations(random.nextInt(possibleContinuations.length))

      results = results + " " + nextWord

      currentKey = getNextKey(currentKey, nextWord)
    }
    
    results
  }

  private def shouldStop(currentKey : String) : Boolean = {
    currentKey.endsWith(".")
  }

  private def getNextKey(currentKey : String, nextWord : String ) : String = {
    val words = currentKey.split(" ")
    words(1) + " " + nextWord
  }

  def createDirectory(source : String): Map[String, ArrayBuffer[String]] = {
    var results : Map[String, ArrayBuffer[String]] = Map()
    val words = source.split(" ")
    for ( i <- 0 to words.length - 3 ) {
      val currentWord = words(i)
      val nextWord = words(i + 1)
      val entryKey = currentWord + " " + nextWord
      val entryValue = words(i + 2)
      if(results.contains(entryKey)) {
        var updatedValue : ArrayBuffer[String] = results(entryKey)
        updatedValue = updatedValue += entryValue
        results.update(entryKey, updatedValue)
      } else {
        results += (entryKey -> ArrayBuffer(entryValue))
      }
    }
    results
  }
}
