package controllers

import play.api.data._
import play.api.data.Forms._
import play.api.mvc._
import service.Bullshitter

object Bullshitter extends Controller {

  def index = Action {
    Ok(views.html.index(""))
  }

  val bullshitForm = Form(
    tuple(
      "source" -> text,
      "startingPoint" -> text
    )
  )

  def generateBullshit() = Action { implicit request =>
    val(source, startingPoint) = bullshitForm.bindFromRequest.get
    Ok(views.html.index(new Bullshitter(scala.util.Random).bullshit(source, startingPoint)))
  }

}
